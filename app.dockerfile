FROM giaohangnhanh_nginx:latest
MAINTAINER transybao28@gmail.com

RUN mkdir /api
WORKDIR /api

COPY ./giaohangnhanh /api
COPY ./package.json /api/package.json
RUN cd /api \
    && npm install \
    && npm audit fix \
    && npm audit fix --force

EXPOSE 3000
CMD [ "npm", "start" ]